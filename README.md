# CV au format HTML / CSS

Impression / PDF conforme au rendu du navigateur.

## Résultat

https://seb-lepers.gitlab.io/cv/

## Crédits
* https://github.com/christiankozalla/dev-resume-template
* https://ionicons.com/

## Build

Mode dev (auto-reload) :
```bash
$ yarn dev
```

Builder le résultat HTML/CSS :
```bash
$ yarn build
```

## Générer le PDF

Pour générer la version PDF (nécessite Google Chrome),
il faut lancer `yarn dev`, puis :

```bash
$ google-chrome --headless --disable-gpu --no-margins --print-to-pdf-no-header --print-to-pdf=output.pdf http://localhost:3000/
```
Le résultat est enregistré dans le fichier `output.pdf`.
